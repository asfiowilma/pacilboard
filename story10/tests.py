from django.test import TestCase
from django.urls import resolve, reverse

from .views import index, signup, user
from django.contrib.auth.views import LoginView, LogoutView 

from django.contrib.auth.models import User

# Create your tests here.
class ViewTest(TestCase):

    def test_root_url_resolves_to_index_view(self):
        found = resolve(reverse('story10:index'))
        self.assertEqual(found.func, index)
    
    def test_index_page_returns_correct_html(self):
        response = self.client.get(reverse('story10:index'))
        html = response.content.decode('utf8')
        self.assertIn('dark', html)
        self.assertTemplateUsed(response, 'story10/index.html')
    
    def test_login_page_returns_correct_html(self):
        response = self.client.get(reverse('story10:login'))
        html = response.content.decode('utf8')
        self.assertIn('In', html)
        self.assertTemplateUsed(response, 'story10/login.html')
    
    def test_logout_page_returns_correct_html(self):
        response = self.client.get(reverse('story10:logout'))
        html = response.content.decode('utf8')
        self.assertIn('out', html)
        self.assertTemplateUsed(response, 'story10/logout.html')

    def test_signup_url_resolves_to_signup_view(self):
        found = resolve(reverse('story10:signup'))
        self.assertEqual(found.func, signup)
    
    def test_signup_page_returns_correct_html(self):
        response = self.client.get(reverse('story10:signup'))
        html = response.content.decode('utf8')
        self.assertIn('Sign Up', html)
        self.assertTemplateUsed(response, 'story10/signup.html')

    def test_user_url_resolves_to_user_view(self):
        found = resolve(reverse('story10:user'))
        self.assertEqual(found.func, user)

class UserTest(TestCase):
    
    def test_user_creation_test(self):
        users = User.objects.all()
        userNow = users.count()

        response = self.client.post(reverse('story10:signup'), data={
            'username': 'nathaniel',
            'password1': 'handsomebastard123',
            'password2': 'handsomebastard123'
        })

        self.assertEqual(users.count(), userNow + 1)

        last_user_registered = users[users.count()-1]

        self.assertEqual(str(last_user_registered.profile), "nathaniel's Profile")
        self.assertEqual(last_user_registered.username, 'nathaniel')
    
    def test_profile_test(self):
        self.client.post(reverse('story10:signup'), data={
            'username': 'nathaniel',
            'password1': 'handsomebastard123',
            'password2': 'handsomebastard123'
        })
        self.client.post(reverse('story10:login'), data={
            'username': 'nathaniel',
            'password': 'handsomebastard123',
        })
        self.client.post(reverse('story10:user'), data={
            'image': 'nathaniel.jpg',
            'fname': 'Nathaniel',
            'lname': 'Droide'
        })
        users = User.objects.all()
        last_user_registered = users[users.count()-1]
        self.assertEqual(last_user_registered.profile.fname, 'Nathaniel')
        self.assertEqual(last_user_registered.profile.lname, 'Droide')



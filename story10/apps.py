from django.apps import AppConfig


class Story10Config(AppConfig):
    name = 'story10'

    def ready(self):
        import story10.models

from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required

from pacilboard.models import snippet
from .forms import EditProfileForm

import random

# Create your views here.
def index(request): 
    snippet.sort()
    context = {
        'snippets': snippet
    }
    return render(request, 'story10/index.html', context)

def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            uname = form.cleaned_data.get('username')
            messages.add_message(request, messages.SUCCESS, f'Successfully registered {uname}. You can now log in.')
            return redirect('story10:login')
    else:
        form = UserCreationForm()

    context = {
        'form': form
    }
    return render(request, 'story10/signup.html', context)

@login_required
def user(request):    
    if request.method == 'POST':
        form = EditProfileForm(request.POST, instance=request.user.profile)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, f'Successfully updated profile.')
            return redirect('story10:user')    
    else: 
        form = EditProfileForm(instance=request.user.profile)

    snippet.sort()
    context = {
        'snippets': snippet,
        'form': form
    }
    return render(request, 'story10/user.html', context)


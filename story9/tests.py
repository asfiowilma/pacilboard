from django.test import TestCase
from django.urls import resolve, reverse

from .views import index, like, top5
from .models import Book

# Create your tests here.
class IndexPageTest(TestCase):

    def test_root_url_resolves_to_index_view(self):
        found = resolve(reverse('story9:index'))
        self.assertEqual(found.func, index)
    
    def test_home_page_returns_correct_html(self):
        response = self.client.get(reverse('story9:index'))
        html = response.content.decode('utf8')
        self.assertIn('RakBooKoo', html)
        self.assertTemplateUsed(response, 'story9/index.html')

    def test_like_url_resolves_to_like_view(self):
        found = resolve(reverse('story9:like'))
        self.assertEqual(found.func, like)

    def test_top5_url_resolves_to_top5_view(self):
        found = resolve(reverse('story9:top5'))
        self.assertEqual(found.func, top5)

class BookModelTest(TestCase):
    def test_book_like(self):
            book1 = Book(id="1", title="Omennnnn", authors="kalex")
            book1.save()

            book2 = Book(id="2", title="Lukisan hororrr", authors="kalex")
            book2.save()

            saved_books = Book.objects.all()
            self.assertEqual(saved_books.count(), 2)

            first_saved_book = saved_books[0]
            second_saved_book = saved_books[1]
            self.assertEqual(first_saved_book.title, 'Omennnnn')
            self.assertEqual(second_saved_book.title, 'Lukisan hororrr')


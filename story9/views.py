from django.shortcuts import render, redirect, reverse
from django.http import JsonResponse
from django.urls import resolve, reverse
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages

from .models import Book


# Create your views here.
def index(request): 
    return render(request, 'story9/index.html')

@csrf_exempt
def like(request): 
    if request.method == 'POST':
        id = request.POST['id']
        title = request.POST['title']
        authors = request.POST['authors']        

        book, status = Book.objects.get_or_create(id=id, title=title, authors=authors)
        if status: 
            # book has never been liked before :(
            book.save()
        else:
            # book is liked already 
            book.likes += 1
            liked = Book(id=id, title=book.title, authors=book.authors, likes=book.likes)
            liked.save()
        return JsonResponse(book.as_dict())
        

def top5(request): 
    top5 = [book.as_dict() for book in Book.objects.order_by('-likes')[:5]]
    return JsonResponse({'top5':top5})


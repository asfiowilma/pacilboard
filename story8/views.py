from django.shortcuts import render

# Create your views here.
def index(request): 
    context = {
        "accordion": [{
            "id": "target1",
            "header": "Current Activities",
            "body": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium deserunt reiciendis iure culpa provident blanditiis ipsa atque sapiente veritatis doloribus?"
        }, 
        {
            "id": "target2",
            "header": "Work Experience",
            "body": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium deserunt reiciendis iure culpa provident blanditiis ipsa atque sapiente veritatis doloribus?"
        }, 
        {
            "id": "target3",
            "header": "Achievements",
            "body": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium deserunt reiciendis iure culpa provident blanditiis ipsa atque sapiente veritatis doloribus?"
        }, 
        {
            "id": "target4",
            "header": "Hobbies & Interests",
            "body": "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium deserunt reiciendis iure culpa provident blanditiis ipsa atque sapiente veritatis doloribus?"
        }, ]
    }
    return render(request, 'story8/index.html', context)
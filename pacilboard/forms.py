from django.forms import ModelForm
from django import forms
from .models import StatusMessage

class Form(ModelForm):
    class Meta:
        model = StatusMessage
        fields = '__all__'
        exclude = ['color']
        widgets = {
            'name': forms.TextInput(
				attrs={
					'class': 'form-control newmsg',
                    'id': 'form_name',
                    'placeholder': 'Enter your name',
					}
				),
            'msg': forms.Textarea(
				attrs={
					'class': 'form-control newmsg',
                    'id': 'form_message',
                    'label': 'Status Message',                    
                    'placeholder': 'What\'s on your mind?',
                    'rows': 3,
					}
				),
        }

# class Color(ModelForm):
#     class Meta:
#         model = StatusMessage
#         fields = '__all__'
#         exclude = ['name', 'msg']
#         widgets = {
#             'color': forms.TextInput(
# 				attrs={
#                     'class': 'change-color',
# 					}
# 				),
#         }
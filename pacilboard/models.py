from django.db import models

# Create your models here.
class StatusMessage(models.Model):
    name = models.CharField(max_length=20)
    msg = models.TextField()
    color = models.CharField(max_length=20, default="burlywood")

snippet = [
        ["Night time sharpens, heightens each sensation",
        "Darkness wakes and stirs imagination",
        "Silently, the senses abandon their defenses"],

        ["Slowly, gently, night unfurls its splendor",
        "Grasp it, sense it, tremulous and tender",
        "Hearing is believing, music is deceiving",
        "Hard as lightening, soft as candlelight",
        "Dare you trust the music of the night?"],

        ["Close your eyes for your eyes will only tell the truth",
        "And the truth isn't what you want to see",
        "In the dark it is easy to pretend",
        "That the truth is what it ought to be"],

        ["Softly, deftly, music shall caress you",
        "Hear it, feel it secretly possess you",
        "Open up your mind, let your fantasies unwind",
        "In this darkness which you know you cannot fight",
        "The darkness of the music of the night"],

        ["Close your eyes",
        "Start a journey to a strange new world",
        "Leave all thoughts of the world you knew before"],

        ["Let the dream begin,", "let your darker side give in"],

        ["I'm here, nothing can harm you",
        "My words will warm and calm you"],

        ["Say you love me every waking moment",
        "Turn my head with talk of summertime",
        "Say you need me with you now and always",
        "Promise me that all you say is true",
        "That's all I ask of you"],

        ["All I want is freedom",
        "A world with no more night",
        "And you, always beside me",
        "To hold me and to hide me"],

        ["Say you need me with you, here beside you",
        "Anywhere you go, let me go too"],

        ["Say you'll share with me one love, one lifetime",
        "Say the word and I will follow you."],
        
        ["Anywhere you go, let me go too",
        "Love me, that's all I ask of you"],

        ["Where in the world",
        "Have you been hiding?",
        "Really - you were perfect"],



]
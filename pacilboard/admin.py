from django.contrib import admin
from .models import StatusMessage

# Register your models here.
admin.site.register(StatusMessage)

# Generated by Django 2.2.7 on 2020-04-13 09:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pacilboard', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='statusmessage',
            name='color',
            field=models.CharField(default='bisque', max_length=20),
        ),
    ]
